#include <iostream>
#include <opencv2/opencv.hpp>

void drawTextCenter(cv::Mat image, const char *text, cv::Point point)
{
	// Text display settings.
	const int fontFace = cv::FONT_HERSHEY_SIMPLEX;
	const double scale = 1.0;
	const int thickness = 1;

	// Calculate text center location.
	int baseline = 0;
	cv::Size size = cv::getTextSize(text, fontFace, scale, thickness, &baseline);
	cv::Point centerText(size.width / 2.0, size.height / 2.0);
	cv::Point center(point.x - centerText.x, point.y - centerText.y);

	// Draw rectangle and text.
	cv::rectangle(image, center + cv::Point(0, baseline), center + cv::Point(size.width, -size.height - baseline), CV_RGB(0, 0, 0), CV_FILLED);
	cv::putText(image, text, center, fontFace, scale, CV_RGB(255, 255, 255), thickness, 8);
}

int main(int argc, char** argv )
{
	const char *windowName = "ASL Interpreter";
	cv::VideoCapture video(0);
	if (!video.isOpened())
	{
		std::cout << "Unable to capture video." << std::endl;
		return 1;
	}

	cv::Mat frame;
	cv::namedWindow(windowName, cv::WINDOW_AUTOSIZE);

	for (;;)
	{
		video >> frame;
		drawTextCenter(frame, "This is where our translation will go.", cv::Point(frame.cols / 2.0, frame.rows - 10));
		cv::imshow(windowName, frame);

		if (cv::waitKey(30) >= 0)
		{
			break;
		}
	}

	return 0;
}
